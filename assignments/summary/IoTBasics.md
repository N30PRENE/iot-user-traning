<h3>#What is IoT?</h3>
IoT stands for interconnection of internet enabled devices or things unlike people only connectivity used in today's internet. Due to huge growth in demand for IoT compliant devices chip makers are having great future ahead. IoT is the short form of Internet of Things. According to cisco and other organizations there will be about 50 billion devices connected using internet in the year 2020. Devices based on ethernet, zigbee, bluetooth and wifi will have huge requirements in consumer electronics segment. Cellular devices based on GSM and LTE also will be available to have connectivity with cellular wireless networks, READ MORE.

<h3>#IoT Applications</h3>
IoT devices are used for following applications:
•  In consumer market for smart home control(lighting,security,comfort),Optimized energy use maintenance.
•  In Industrial market for smartmeters, Wear-out sensing devices, Manufacturing control, Climate control
•  In Automotive industries for parking, traffic flow control, Anti-theft devices, Location tracking etc.
•  In Environmental for species tracking, weather prediction and resource management.
•  In agriculture market for crop management and soil analysis.
•  In military for resource allocation, threat analysis, troop monitoring etc.
•  In medical industry for wearable devices, implanted devices and telehealth services.
•  In retail for product tracking, Inventory control, focused marketing, etc.

<h3>#IoT Entities</h3>
There are two major subsystems involved in the IoT network viz. front end part and back end part. Front end is mainly consists of IoT sensors which are MEMS based. It includes optical sensors, light sensors, gesture and proximity sensors, touch and fingerprint sensors, pressure sensors and more. 
Back end consists of cellular, wireless and wired networks which are interfaced with IoT devices. The devices will report to the central servers and also interact with databases in the backbone network. Routers and gateways are part of the wireless backbone networks.

</h3>#IoT Protocol layers</h3>
As the standard has not been finalized for IoT but one can think of IoT having 4 protocol layers as shown in the figure-2. Sensing and Identification include various smart sensor devices based on GPS, RFID, WiFi etc. Network connectivity layer is based on wired and wireless network such as WLAN, WPAN, WMAN, ethernet, optical fiber and more.Other two layers are information Processing layer and application layer.



</h3>#Cellular IoT</h3>
Wireless cellular companies are working towards providing collectivity and enhancement to existing wireless devices to support emerging IoT market. 



<h3>#IoT Device and components</h3>
The IoT device mainly consists of battery for providing power. It should have long life approx. about 10 years. The parts include interfacing with sensors and connectivity with wireless and wired network. Hence it include small part of physical layer and also upper protocol layers to interface with application layer. Devices should support both IPV4 and IPV6 based IP protocols. IoT devices must have receiver sensitivity atleast 20dB better than non-IoT devices. The IoT devices should be cheaper about less than $10. Refer IoT components for more information.

<h3>#IoT − Key Features</h3>
The most important features of IoT include artificial intelligence, connectivity, sensors, active engagement, and small device use. A brief review of these features is given below −

**1. AI −** IoT essentially makes virtually anything “smart”, meaning it enhances every aspect of life with the power of data collection, artificial intelligence algorithms, and networks. This can mean something as simple as enhancing your refrigerator and cabinets to detect when milk and your favorite cereal run low, and to then place an order with your preferred grocer.

**2. Connectivity −** New enabling technologies for networking, and specifically IoT networking, mean networks are no longer exclusively tied to major providers. Networks can exist on a much smaller and cheaper scale while still being practical. IoT creates these small networks between its system devices.

**3. Sensors −** IoT loses its distinction without sensors. They act as defining instruments which transform IoT from a standard passive network of devices into an active system capable of real-world integration.

**4. Active Engagement −** Much of today's interaction with connected technology happens through passive engagement. IoT introduces a new paradigm for active content, product, or service engagement.

**5. Small Devices −** Devices, as predicted, have become smaller, cheaper, and more powerful over time. IoT exploits purpose-built small devices to deliver its precision, scalability, and versatility.

<h3>#IoT − Advantages</h3>
The advantages of IoT span across every area of lifestyle and business. Here is a list of some of the advantages that IoT has to offer −

**1. Improved Customer Engagement −** Current analytics suffer from blind-spots and significant flaws in accuracy; and as noted, engagement remains passive. IoT completely transforms this to achieve richer and more effective engagement with audiences.

**2. Technology Optimization −** The same technologies and data which improve the customer experience also improve device use, and aid in more potent improvements to technology. IoT unlocks a world of critical functional and field data.

**3. Reduced Waste −** IoT makes areas of improvement clear. Current analytics give us superficial insight, but IoT provides real-world information leading to more effective management of resources.

**4. Enhanced Data Collection −** Modern data collection suffers from its limitations and its design for passive use. IoT breaks it out of those spaces, and places it exactly where humans really want to go to analyze our world. It allows an accurate picture of everything.

<h3>#IoT − Disadvantages</h3>
Though IoT delivers an impressive set of benefits, it also presents a significant set of challenges. Here is a list of some its major issues −

**1. Security −** IoT creates an ecosystem of constantly connected devices communicating over networks. The system offers little control despite any security measures. This leaves users exposed to various kinds of attackers.

**2. Privacy −** The sophistication of IoT provides substantial personal data in extreme detail without the user's active participation.

**3. Complexity − **Some find IoT systems complicated in terms of design, deployment, and maintenance given their use of multiple technologies and a large set of new enabling technologies.

**4. Flexibility −** Many are concerned about the flexibility of an IoT system to integrate easily with another. They worry about finding themselves with several conflicting or locked systems.

**5. Compliance −** IoT, like any other technology in the realm of business, must comply with regulations. Its complexity makes the issue of compliance seem incredibly challenging when many consider standard software compliance a battle.

<h3>Internet of Things - Hardware</h3>
The hardware utilized in IoT systems includes devices for a remote dashboard, devices for control, servers, a routing or bridge device, and sensors. These devices manage key tasks and functions such as system activation, action specifications, security, communication, and detection to support-specific goals and actions.

**1. IoT − Sensors**
The most important hardware in IoT might be its sensors. These devices consist of energy modules, power management modules, RF modules, and sensing modules. RF modules manage communications through their signal processing, WiFi, ZigBee, Bluetooth, radio transceiver, duplexer, and BAW.


**2. Wearable Electronics**
Wearable electronic devices are small devices worn on the head, neck, arms, torso, and feet.
**Current smart wearable devices include −**

**Head −** Helmets, glasses
**Neck −** Jewelry, collars
**Arm −** Watches, wristbands, rings
T**orso −** Clothing, backpacks
**Feet −** Socks, shoes


**3. Standard Devices**
The desktop, tablet, and cellphone remain integral parts of IoT as the command center and remotes.

The desktop provides the user with the highest level of control over the system and its settings.

The tablet provides access to the key features of the system in a way resembling the desktop, and also acts as a remote.

The cellphone allows some essential settings modification and also provides remote functionality.

Other key connected devices include standard network devices like routers and switches.
